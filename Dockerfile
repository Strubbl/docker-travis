FROM ruby:alpine
LABEL maintainer="Strubbl <Strubbl-Dockerfile@linux4tw.de>"

ENV TRAVIS_HOME "/home/travis"
VOLUME "${TRAVIS_HOME}"

RUN mkdir -p "${TRAVIS_HOME}" && \
    apk add --no-cache --update gcc musl-dev make git && \
    gem install travis -v 1.8.9 --no-rdoc --no-ri && \
    apk del --rdepends gcc musl-dev make && \
    addgroup travis && \
    adduser -D -h "${TRAVIS_HOME}" -G travis travis && \
    chown -R travis:travis "${TRAVIS_HOME}"
USER travis

CMD ["travis"]

